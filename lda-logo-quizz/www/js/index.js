function get_levels(){
  app.request({
    url: "../base.json",
    method: "GET",
    dataType: "json",
    beforeSend: function() {
      app.dialog.preloader('Chargement', 'blue');
    },
    success: function(res) {
      const numberOfGroupLevels = res.length;
      
      for(let index = 0; index < numberOfGroupLevels; index++){
        var logo = res[index].object.photo;
          $('.level__selection').append(`
          <div class="col-30 level-icon" id="level-${index}" style="padding:2vw;">
            <a href="/play/${index}">
              <img src="${logo}" class="level">
            </a>
          </div>
        `)
        if(sessionStorage.getItem(index)){
          $('#level-'+index).append(`
            <img src="../assets/valide.png" class="level-done">
          `)
        }
      }
      app.dialog.close();
    }
  })
};


function get_logo(id) {
    app.request({
      url: "../base.json", //URL de L'api
      method: "GET", // Méthode
      dataType: "json", // Important, sinon vous allez récupérer un string et non un objet
      beforeSend: function () {
        // Avant de récupérer mes datas, j'affiche un loader
        //(important quand on fait un traitement pour montrer qu'il est en cours +  empêcher les impatients de cliquer partout pendant le process !)
        app.dialog.preloader("Trouvez le sport associé au logo");
      },
      success: async function (res) {
        var logo = res[id].object.photo
        $('.logo-container').append(`
          <div class="box">
            <div style="height: 80%;">
              <img class="logo" src="${logo}"/>
            </div>
            <div class="group">
              <input id="${id}" class="answer-user" type="text">
              <img src="../assets/coche.png" class="answer-validate" onclick="checkAnswer(${id})">
            </div>
          </div>
        `)
        setTimeout(function() {
          app.dialog.close();
        }, 2000);
        if(sessionStorage.getItem(id)){
          $('.group').append(`
            <img src="../assets/valide.png" class="done">
          `)
        }
      },

      error: function(res){
        console.log(res)
      }
    })
};

function checkAnswer(id){
  app.request({
    url: "../base.json", //URL de L'api
    method: "GET", // Méthode
    dataType: "json", // Important, sinon vous allez récupérer un string et non un objet
    success: async function (res) {
      var userAnswer = cleanword(document.getElementById(id).value)
      var goodSport = res[id].object.sport
      if(goodSport.includes(userAnswer)){
        sessionStorage.setItem(id, true)
        $('.content').append(`
          <img src="../assets/valide.png" class="validated">
        `)
        $(id).append(`
          <img src="../assets/coche.png" class="done">
        `)
        app.dialog.alert('Bonne réponse !');
      } else {
        app.dialog.alert('Mauvaise réponse !');
      }
    }
  })
};


function cleanword(word) {
  word = word.normalize('NFD')
    .split(' ').join('')
    .replace(/[\u0300-\u036f]/g, "")
    .replace(/[^a-zA-Z0-9 ]/g, '')
  return word
}

