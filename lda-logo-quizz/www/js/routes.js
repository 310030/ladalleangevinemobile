var routes = [
  {
    path: '/',
    url: './index.html',
  },
  {
    name: 'home',
    path: '/home/',
    url: './pages/home.html',
  },
  {
    name: 'start',
    path: '/start/',
    url: './pages/start.html',
    on: {
      pageInit:function(e, page){
        var router = this;
        var app = router.app;
        get_levels();
      },
      pageAfterIn: function(e, page) {
        var router = this;
        var app = router.app;
        
        
      }
    }
  },
  {
    name: 'play',
    path: '/play/:id',
    url: './pages/play.html',
    on: {
      pageInit: function (e, page) {
        var router = this;
        var app = router.app;
        var groupLevelsId = page.route.params.id;

        get_logo(groupLevelsId);
      },
      pageAfterIn: function(e, page) {
        var router = this;
        var app = router.app;
        var groupLevelsId = page.route.params.id;
      }
    }
  },
  // Default route (404 page). MUST BE THE LAST
  {
    path: '(.*)',
    url: './pages/404.html',
  },
];
